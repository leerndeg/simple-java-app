#!/bin/bash

# inputs
#  - repository names(comma sep)
#  - count of snapshots to leave in every branch


REPOSITORY_NAMES=$1
COUNT_TO_REMAIN=$2

IFS=',' read -ra parts <<< "$REPOSITORY_NAMES"
for repository in "${parts[@]}"
do
    repository=$(echo "${parts[$repository]}" | tr -d '[:space:]')
    json_result=$(jf rt search "$repository/*" --recursive=false --include-dirs)

    branches=$(echo "$json_result" | jq -r '.[] | .path' | awk -F'/' '{print $(NF)}')
    while IFS= read -r branch; do
        if [[ -n "$branch" ]]; then
            jf rt delete "$repository/$branch/*" --sort-by=created --offset=$COUNT_TO_REMAIN --recursive=false  --quiet=true
        fi
    done <<< "$branches"
done

# create 2 jobs