#!/bin/bash

cd target
filename=$(ls ./ | head -n 1)
timestamp="$(echo "$filename"   | cut -d'-' -f1)"
commit=$1
commit=${commit:0:5}
branchName=$(git branch   | cut -d'*' -f2 | cut -d' ' -f2)

new_file_name="${filename%.jar}$commit.jar"
jf rt u "*.jar" "andrii-maven-repo-snapshots/$branchName/$timestamp-de-$commit/$new_file_name"

echo "Uploaded file name is $new_file_name"

