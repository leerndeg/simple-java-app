FROM centos:8

# Fix for Error: Failed to download metadata for repo 'appstream': Cannot prepare internal mirrorlist: No URLs in mirrorlist
RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*


RUN yum update -y

# Install net-tools
RUN yum install -qy net-tools 

# Install unzip
RUN yum install -qy unzip

# Install Java 11
RUN curl -O https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz
RUN tar zxvf openjdk-11.0.1_linux-x64_bin.tar.gz
RUN mv jdk-11.0.1 /usr/local/
ENV JAVA_HOME /usr/local/jdk-11.0.1
ENV PATH $PATH:$JAVA_HOME/bin


# Install Maven 3.5
RUN curl -O https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.5.0/apache-maven-3.5.0-bin.tar.gz
RUN tar zxvf apache-maven-3.5.0-bin.tar.gz
RUN mv apache-maven-3.5.0 /usr/local/
ENV M2_HOME /usr/local/apache-maven-3.5.0
ENV PATH $PATH:$M2_HOME/bin

# Install OpenJfx
RUN curl -O https://download2.gluonhq.com/openjfx/17.0.10/openjfx-17.0.10_linux-x64_bin-sdk.zip
RUN unzip openjfx-17.0.10_linux-x64_bin-sdk.zip
RUN cp -arf javafx-sdk-17.0.10/lib/* ${JAVA_HOME}/lib/
ENV PATH_TO_FX ${JAVA_HOME}/lib/

CMD ["/bin/bash"]