FROM mcr.microsoft.com/windows/servercore:ltsc2019

# $ProgressPreference: https://github.com/PowerShell/PowerShell/issues/2138#issuecomment-251261324
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]


RUN Write-Host 'Enabling TLS 1.2 (https://githubengineering.com/crypto-removal-notice/) ...'; \
	$tls12RegBase = 'HKLM:\\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2'; \
	if (Test-Path $tls12RegBase) { throw ('"{0}" already exists!' -f $tls12RegBase) }; \
	New-Item -Path ('{0}/Client' -f $tls12RegBase) -Force; \
	New-Item -Path ('{0}/Server' -f $tls12RegBase) -Force; \
	New-ItemProperty -Path ('{0}/Client' -f $tls12RegBase) -Name 'DisabledByDefault' -PropertyType DWORD -Value 0 -Force; \
	New-ItemProperty -Path ('{0}/Client' -f $tls12RegBase) -Name 'Enabled' -PropertyType DWORD -Value 1 -Force; \
	New-ItemProperty -Path ('{0}/Server' -f $tls12RegBase) -Name 'DisabledByDefault' -PropertyType DWORD -Value 0 -Force; \
	New-ItemProperty -Path ('{0}/Server' -f $tls12RegBase) -Name 'Enabled' -PropertyType DWORD -Value 1 -Force; \
	Write-Host 'Complete.'

RUN [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;

ENV JAVA_HOME C:\\openlogic-openjdk-11.0.22+7-windows-x64
RUN $newPath = ('{0}\bin;{1}' -f $env:JAVA_HOME, $env:PATH); \
	Write-Host ('Updating PATH: {0}' -f $newPath); \
	setx /M PATH $newPath; \
	Write-Host 'Complete.'


ENV JAVA_URL https://builds.openlogic.com/downloadJDK/openlogic-openjdk/11.0.22+7/openlogic-openjdk-11.0.22+7-windows-x64.zip

RUN Write-Host ('Downloading {0} ...' -f $env:JAVA_URL); \
	Invoke-WebRequest -Uri $env:JAVA_URL -OutFile 'openjdk.zip'; \
	\
	Write-Host 'Expanding ...'; \
	New-Item -ItemType Directory -Path C:\temp | Out-Null; \
	Expand-Archive openjdk.zip -DestinationPath C:\temp; \
	Move-Item -Path C:\temp\* -Destination $env:JAVA_HOME; \
	Remove-Item C:\temp; \
	\
	Write-Host 'Removing ...'; \
	Remove-Item openjdk.zip -Force; \
	\
	Write-Host 'Complete.'


ENV MAVEN_HOME C:\\apache-maven-3.5.0
RUN $newPath = ('{0}\bin;{1}' -f $env:MAVEN_HOME, $env:PATH); \
	Write-Host ('Updating PATH: {0}' -f $newPath); \
	setx /M PATH $newPath; \
	Write-Host 'Complete.'

ARG MAVEN_VERSION=3.5.0
ARG MAVEN_URL=https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/${MAVEN_VERSION}/apache-maven-3.5.0-bin.zip

RUN Write-Host ('Downloading {0} ...' -f ${MAVEN_URL}); \
	Invoke-WebRequest -Uri ${env:MAVEN_URL} -OutFile 'C:/apache-maven.zip' ; \
	Write-Host 'Expanding ...'; \
	New-Item -ItemType Directory -Path 'C:/temp' | Out-Null ; \
	Expand-Archive -Path 'C:/apache-maven.zip' -Destination 'C:/temp' ; \
	Move-Item C:/temp/apache-maven-${env:MAVEN_VERSION} $env:MAVEN_HOME ; \
	Write-Host 'Removing ...'; \
	Remove-Item C:\temp; \
	Remove-Item 'C:/apache-maven.zip' -Force ; \
	\
	Write-Host 'Complete.'


ENV JAVA_FX_HOME=${JAVA_HOME}\\lib
RUN $newPath = ('{0}\bin;{1}' -f $env:JAVA_FX_HOME, $env:PATH); \
	Write-Host ('Updating PATH: {0}' -f $newPath); \
	setx /M PATH $newPath; \
	Write-Host 'Complete.'
ARG JAVA_FX_URL=https://download2.gluonhq.com/openjfx/17.0.10/openjfx-17.0.10_windows-x64_bin-sdk.zip

RUN Write-Host ('Downloading {0} ...' -f ${JAVA_FX_URL}); \
	Invoke-WebRequest -Uri ${env:JAVA_FX_URL} -OutFile 'C:/java-fx.zip' ; \
	Write-Host 'Expanding ...'; \
	New-Item -ItemType Directory -Path 'C:/temp' | Out-Null ; \
	Expand-Archive -Path 'C:/java-fx.zip' -Destination 'C:/temp' ; \
	Move-Item 'C:/temp/javafx-sdk-17.0.10/lib' $env:JAVA_FX_HOME; \
	Write-Host 'Removing ...'; \
	Remove-Item C:\temp -Recurse ; \
	Remove-Item 'C:/java-fx.zip' -Force ; \
	\
	Write-Host 'Complete.'


# install git; add to path

ENV GIT_HOME C:\\git
ARG GIT_URL=https://github.com/git-for-windows/git/releases/download/v2.38.0.windows.1/MinGit-2.38.0-64-bit.zip

RUN $newPath = ('{0}\cmd;{1}' -f $env:GIT_HOME, $env:PATH); \
	Write-Host ('Updating PATH: {0}' -f $newPath); \
	setx /M PATH $newPath; \
	Write-Host 'Complete.'
RUN Invoke-WebRequest -Uri ${env:GIT_URL} -OutFile 'C:\\MinGit.zip'; \
	Expand-Archive c:\\MinGit.zip -DestinationPath C:\\git; \
	\
	Write-Host 'Complete.'

ARG GIT_LFS_URL=https://github.com/git-lfs/git-lfs/releases/download/v3.5.1/git-lfs-windows-amd64-v3.5.1.zip

RUN Write-Host ('Downloading {0} ...' -f ${GIT_LFS_URL}); \
	Invoke-WebRequest -Uri ${env:GIT_LFS_URL} -OutFile 'C:\\git-lfs.zip'; \
	Write-Host 'Expanding ...'; \
	New-Item -ItemType Directory -Path 'C:/temp' | Out-Null ; \
	Expand-Archive -Path C:\\git-lfs.zip -DestinationPath 'C:/temp'; \
	Move-Item 'C:/temp/git-lfs-3.5.1/git-lfs.exe' $env:GIT_HOME\\cmd; \
	Remove-Item C:\temp -Recurse ; \
	Remove-Item 'C:/git-lfs.zip' -Force ; 

RUN Write-Host 'javac --version'; javac --version; \
	Write-Host 'java --version'; java --version; \
	Write-Host 'mvn --version'; mvn --version; \
	Write-Host 'echo $Env:JAVA_FX_HOME'; echo $Env:JAVA_FX_HOME; \
	Write-Host 'git --version'; git --version; \
	Write-Host 'git lfs install'; git lfs install; 


CMD ["powershell"]
